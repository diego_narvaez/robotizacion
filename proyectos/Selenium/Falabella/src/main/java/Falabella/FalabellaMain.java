package Falabella;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FalabellaMain {
	
	/*
	 * Inicializadores
	 */
	
	public static WebDriver driver;
	private final static String chromeDriver="webdriver.chrome.driver";
	private final static String ubicacionChromeDriver="\\src\\main\\resources\\chromedriver.exe";
	
	
	/*
	 * Ubicaciones Selenium
	 */
	
	private final static String URL_Pagina = "https://www.falabella.com/falabella-cl/";
	private final static String ID_Categorias = "hamburgerMenu";
	private final static String ID_Tecnologias = "item-3";
	private final static String XPATH_Videojuegos = "//*[@id='header']/nav/div[1]/div[1]/div[1]/div/section[1]/div/section[4]/div/div[1]/ul[5]/div[1]/div[1]/a";
	private final static String XPATH_PlayStation = "//*[@id='main']/div[1]/div/div[2]/div[1]/nav/div[3]";
	private final static String ID_Producto = "testId-pod-8158922";
	private final static String ID_VerProducto= "testId-Pod-action-8158922";
	private final static String ID_AnadirAlCarro="buttonForCustomers";
	private final static String XPATH_CantidadProducto= "//*[@id='__next']/div/div/div/div/div/div/div[2]/div[1]/div/div[1]/div/div[3]/div/div";
	private final static String XPATH_BTN_Mas="//*[@id='__next']/div/div/div/div/div/div/div[2]/div[1]/div/div[1]/div/div[3]/div/button[2]";
	private final static String ID_VerBolsa="linkButton";
	
	public static void main(String[] args) throws InterruptedException {
		ejecutarAutomatizacion();
	}

	private static void ejecutarAutomatizacion() throws InterruptedException {
		/*
		 * Importante Version de Chrome 83
		 */
		WebDriver driver = getDriver();
		driver.get(URL_Pagina);espera();
		driver.findElement(By.id(ID_Categorias)).click();
		driver.findElement(By.id(ID_Tecnologias)).click();
		driver.findElement(By.xpath(XPATH_Videojuegos)).click();
		espera();
		driver.findElement(By.xpath(XPATH_PlayStation)).click();
		Actions actions = new Actions(driver);
		WebElement producto = driver.findElement(By.id(ID_Producto));
		actions.moveToElement(producto).perform();
		espera();
		driver.findElement(By.id(ID_VerProducto)).click();
		espera();
		driver.findElement(By.id(ID_AnadirAlCarro)).click();
		int cantidadEsperada = 3;
		int cantidad = Integer.parseInt(driver.findElement(By.xpath(XPATH_CantidadProducto)).getText());
		while(cantidad != cantidadEsperada) {
			driver.findElement(By.xpath(XPATH_BTN_Mas)).click();
			espera();
			cantidad = Integer.parseInt(driver.findElement(By.xpath(XPATH_CantidadProducto)).getText());
		}
		driver.findElement(By.id(ID_VerBolsa)).click();
		espera();
		driver.quit();
		
	}

	private static WebDriver getDriver() {
		if (driver == null) {
				System.setProperty(chromeDriver, System.getProperty("user.dir")+ubicacionChromeDriver);
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				driver.manage().deleteAllCookies();
				driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
		}
		return driver;
	}
	
	static void espera() throws InterruptedException{
		Thread.sleep(2000);
	}
}
