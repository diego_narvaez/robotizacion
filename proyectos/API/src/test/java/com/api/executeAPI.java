package com.api;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpStatus;
import org.hamcrest.core.Is;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class executeAPI {
	@Test
	public void getTest1() {
		Response response = null;
		RestAssured.baseURI = "https://api.github.com/gists";
		RequestSpecification request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("id", "33");
		request.body(requestParams);
		response = request.get("https://api.github.com/gists");
		Assert.assertEquals(response.getStatusCode(), 200);
		response.then().assertThat().body("public[0]", Is.is(true));
	}
	
	@Test
	public void getTest2() {
		boolean status;
		String URL = "https://api.github.com/gists/33/comments";
		Response response = null;
		RequestSpecification request = RestAssured.given();
		response = request.get(URL);
		response.then().assertThat().statusCode(HttpStatus.SC_OK);
		if(response.getTime() <500) {
			status = true;
		}else {
			status = false;
		}
		assertTrue(status);

	}
	@Test
	public void getTest3() throws IOException {
		URL url = new URL("https://api.github.com/gists/33/comments");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

		String input = "{\"body\":\"ejemplo\"}";

		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			System.out.println("Error : HTTP Codigo de error: "
				+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

		conn.disconnect();
	}
	
}
